#include "graph.h"

#include <cassert>
#include <iostream>

int main()
{
  oak::graph_t<int> g;
  assert(g.num_nodes() == 0);
  assert(g.num_edges() == 0);

  g.add_node("A", 3);
  assert(g.num_nodes() == 1);
  assert(g.has_node("A"));
  assert(g.value_of("A") == 3);

  auto g2 = oak::graph_t<int>::add_node(g, "B", 5);
  assert(g.num_nodes() == 1);
  assert(g2.num_nodes() == 2);

  g.add_node("B", 5);
  assert(g.num_nodes() == 2);
  assert(g.has_node("B"));
  assert(g.value_of("B") == 5);

  auto g3 = oak::graph_t<int>::add_edge(g, "A", "B");
  assert(g.num_edges() == 0);
  assert(g3.num_edges() == 1);

  g.add_edge("A", "B");
  assert(g.num_edges() == 1);

  for(const auto it : g.ids_bfs("A"))
    std::cout << it << std::endl;

  auto g4{g};
  assert(g4 == g);


  return 0;
}
