#pragma once

#include <boost/graph/adjacency_list.hpp>
#include <boost/functional/hash.hpp>
#include <boost/range/iterator_range.hpp>

#include <deque>
#include <functional>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <utility>


namespace oak { template <typename T> struct graph_t; }

namespace oak::graph {

template <typename T>
struct node_t
{
  T value;
  std::string id;
};

template <typename T>
struct id_iter_bfs_t
{
  id_iter_bfs_t(graph_t<T>&);
  id_iter_bfs_t(graph_t<T>&, const typename graph_t<T>::node_desc);

  void operator++();
  auto operator*() -> std::string&;
  auto operator==(const id_iter_bfs_t<T>& other) const;
  auto operator!=(const id_iter_bfs_t<T>& other) const;

  graph_t<T>& graph;
  std::deque<typename graph_t<T>::node_desc> todo;
};

template <typename T>
id_iter_bfs_t<T>::id_iter_bfs_t(graph_t<T>& g)
: graph{g}
{}

template <typename T>
id_iter_bfs_t<T>::id_iter_bfs_t(graph_t<T>& g, const typename graph_t<T>::node_desc start)
: graph{g}
, todo{start}
{}

template <typename T>
auto id_iter_bfs_t<T>::operator*() -> std::string&
{
  return graph.graph[todo.front()].id;
}

template <typename T>
void id_iter_bfs_t<T>::operator++()
{
  if (todo.empty())
    return;

  const auto curr = todo.front();
  todo.pop_front();

  for (const auto& nb : boost::make_iterator_range(boost::adjacent_vertices(curr, graph.graph)))
    todo.push_back(nb);
}

template <typename T>
auto id_iter_bfs_t<T>::operator==(const id_iter_bfs_t<T>& other) const
{
  return (graph == other.graph) && (todo == other.todo);
}

template <typename T>
auto id_iter_bfs_t<T>::operator!=(const id_iter_bfs_t<T>& other) const
{
  return !(*this == other);
}


template <typename T>
struct id_iter_range_t
{
  id_iter_range_t(const struct id_iter_bfs_t<T>& b, const struct id_iter_bfs_t<T>& e)
  : b{b}, e{e} {}

  auto begin() -> struct id_iter_bfs_t<T> { return b; }
  auto end()   -> struct id_iter_bfs_t<T> { return e; }

  struct id_iter_bfs_t<T> b, e;
};

} // namespace oak::graph


namespace oak {

template <typename T>
struct graph_t
{
  using adj_list  = boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, graph::node_t<T>>;
  using node_desc = typename adj_list::vertex_descriptor;
  using edge_desc = typename adj_list::edge_descriptor;
  using node_id   = std::string;
  using edge_id   = std::pair<node_id, node_id>;
  using edge_hash = boost::hash<edge_id>;
  using node_desc_map = std::unordered_map<node_id, node_desc>;
  using edge_desc_map = std::unordered_map<edge_id, edge_desc, edge_hash>;

  using id_iter_bfs   = graph::id_iter_bfs_t<T>;
  using id_iter_range = graph::id_iter_range_t<T>;

  graph_t() = default;
  graph_t(const graph_t<T>& other);

  auto operator==(const graph_t<T>& other) const -> bool;

  auto num_nodes() const -> std::size_t;
  auto num_edges() const -> std::size_t;

  auto has_node(const std::string&)                     const -> bool;
  auto has_edge(const std::string&, const std::string&) const -> bool;

  void add_node(const std::string&, const T&);
  void add_edge(const std::string&, const std::string&);

  auto value_of(const std::string&)       ->       T&;
  auto value_of(const std::string&) const -> const T&;

  auto ids_bfs(const std::string&) -> id_iter_range;


  static auto add_node(const graph_t<T>&, const std::string&, const T&)           -> graph_t<T>;
  static auto add_edge(const graph_t<T>&, const std::string&, const std::string&) -> graph_t<T>;

  static auto value_of(const graph_t<T>&, const std::string&) -> T;


  adj_list graph;
  node_desc_map nodesMap;
  edge_desc_map edgesMap;
};


template <typename T>
graph_t<T>::graph_t(const graph_t<T>& other)
: graph{other.graph}
, nodesMap{other.nodesMap}
, edgesMap{other.edgesMap}
{}

template <typename T>
auto graph_t<T>::operator==(const graph_t<T>& other) const -> bool
{
  return (nodesMap == other.nodesMap) && (edgesMap == other.edgesMap);
}

template <typename T>
auto graph_t<T>::num_nodes() const -> std::size_t
{
  return boost::num_vertices(graph);
}

template <typename T>
auto graph_t<T>::num_edges() const -> std::size_t
{
  return boost::num_edges(graph);
}

template <typename T>
auto graph_t<T>::has_node(const std::string& id) const -> bool
{
  return (nodesMap.find(id) != nodesMap.end());
}

template <typename T>
auto graph_t<T>::has_edge(const std::string& a, const std::string& b) const -> bool
{
  return (edgesMap.find({a, b}) != edgesMap.end());
}

template <typename T>
void graph_t<T>::add_node(const std::string& id, const T& value)
{
  if (has_node(id)) throw std::invalid_argument{"node exists"};

  auto newNode = boost::add_vertex(graph);

  graph[newNode] = graph::node_t<T>{value, id};
  nodesMap.insert({id, newNode});
}

template <typename T>
void graph_t<T>::add_edge(const std::string& a, const std::string& b)
{
  auto newEdge = boost::add_edge(nodesMap.at(a), nodesMap.at(b), graph).first;
  auto edgeId = std::make_pair(a, b);

  edgesMap.insert({{a, b}, newEdge});
}

template <typename T>
auto graph_t<T>::value_of(const std::string& id) -> T&
{
  if (!has_node(id)) throw std::invalid_argument{"node not found"};

  return graph[nodesMap.at(id)].value;
}

template <typename T>
auto graph_t<T>::value_of(const std::string& id) const -> const T&
{
  if (!has_node(id)) throw std::invalid_argument{"node not found"};

  return graph[nodesMap.at(id)].value;
}

template <typename T>
auto graph_t<T>::ids_bfs(const std::string& id) -> id_iter_range
{
  if (!has_node(id)) throw std::invalid_argument{"node not found"};

  return id_iter_range(id_iter_bfs{*this, nodesMap.at(id)}, id_iter_bfs{*this});
}


template <typename T>
auto graph_t<T>::add_node(const graph_t<T>& g, const std::string& id, const T& value) -> graph_t<T>
{
  graph_t<T> res{g};
  res.add_node(id, value);

  return res;
}

template <typename T>
auto graph_t<T>::add_edge(const graph_t<T>& g, const std::string& a, const std::string& b) -> graph_t<T>
{
  graph_t<T> res{g};
  res.add_edge(a, b);

  return res;
}

template <typename T>
auto value_of(const graph_t<T>& g, const std::string& id) -> T
{
  return g.value_of(id);
}


} // namespace oak
